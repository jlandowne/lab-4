/****************************************************************************
 Template header file for Hierarchical Sate Machines AKA StateCharts
 02/08/12 adjsutments for use with the Events and Services Framework Gen2
 3/17/09  Fixed prototpyes to use Event_t
 ****************************************************************************/

#ifndef ButtonDBSM_H
#define ButtonDBSM_H
#include "ES_Events.h"
#include "ES_Types.h"


// typedefs for the states
// State definitions for use with the query function
typedef enum { Debouncing, Ready2Sample} ButtonDBState_t ;

// Public Function Prototypes

bool InitButtonDBSM(uint8_t Priority);
bool PostButtonDBSM(ES_Event_t);
ButtonDBState_t QueryButtonDBSM ( void );
ES_Event_t RunButtonDBSM(ES_Event_t );

bool CheckButtonEvents(void);

#endif /*MorseElements_H */

