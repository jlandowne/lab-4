/****************************************************************************

  Header file for Decode Morse
  based on the Gen 2 Events and Services Framework

 ****************************************************************************/

#ifndef DecodeMorse_H
#define DecodeMorse_H

#include "ES_Types.h"
#include "ES_Events.h"

// Public Function Prototypes
bool InitDecodeMorse(uint8_t Priority);
bool PostDecodeMorse(ES_Event_t ThisEvent);
ES_Event_t RunDecodeMorse(ES_Event_t ThisEvent);

#endif 

