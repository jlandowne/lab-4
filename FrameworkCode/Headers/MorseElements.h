/****************************************************************************
 Template header file for Hierarchical Sate Machines AKA StateCharts
 02/08/12 adjsutments for use with the Events and Services Framework Gen2
 3/17/09  Fixed prototpyes to use Event_t
 ****************************************************************************/

#ifndef MorseElements_H
#define MorseElements_H
#include "ES_Events.h"
#include "ES_Types.h"


// typedefs for the states
// State definitions for use with the query function
typedef enum { InitMorseElements, CalWaitForRise, CalWaitForFall, EOC_WaitRise, 
EOC_WaitFall, DecodeWaitRise, DecodeWaitFall} MorseElementsState_t ;

// Public Function Prototypes

bool InitializeMorseElementsSM(uint8_t Priority);
bool PostMorseElementsSM(ES_Event_t);
MorseElementsState_t QueryMorseElementsSM ( void );
ES_Event_t RunMorseElementsSM(ES_Event_t );

bool CheckMorseElements(void);

#endif /*MorseElements_H */

