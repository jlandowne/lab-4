#ifndef ShiftRegisterWrite_H
#define ShiftRegisterWrite_H
  void SR_Init(void);
  uint8_t SR_GetCurrentRegister(void);
  void SR_Write(uint8_t NewValue);
  uint8_t SR_Read(void);
  uint8_t Button_Read(void);
#endif