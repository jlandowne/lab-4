/****************************************************************************
 Module
   ButtonDBSM.c

 Revision
   1.0.1

 Description
   This is a template file for implementing flat state machines under the
   Gen2 Events and Services Framework.

 Notes

 History
 When           Who     What/Why
 -------------- ---     --------
 01/15/12 11:12 jec      revisions for Gen2 framework
 11/07/11 11:26 jec      made the queue static
 10/30/11 17:59 jec      fixed references to CurrentEvent in RunTemplateSM()
 10/23/11 18:20 jec      began conversion from SMTemplate.c (02/20/07 rev)
****************************************************************************/
/*----------------------------- Include Files -----------------------------*/
/* include header files for this state machine as well as any machines at the
   next lower level in the hierarchy that are sub-machines to this machine
*/
#include "ES_Configure.h"
#include "ES_Framework.h"
#include "ButtonDBSM.h"
#include "ShiftRegisterWrite.h"
#include "MorseElements.h"
#include "DecodeMorse.h"

#define DEBOUNCE_TIME 250 //debounce time in ms

/*----------------------------- Module Defines ----------------------------*/

/*---------------------------- Module Functions ---------------------------*/
/* prototypes for private functions for this machine.They should be functions
   relevant to the behavior of this state machine
*/
static uint8_t LastButtonState;

/*---------------------------- Module Variables ---------------------------*/
// everybody needs a state variable, you may need others as well.
// type of state variable should match htat of enum in header file
static ButtonDBState_t CurrentState;

// with the introduction of Gen2, we need a module level Priority var as well
static uint8_t MyPriority;

/*------------------------------ Module Code ------------------------------*/
/****************************************************************************
 Function
     InitButtonDBSM

 Parameters
     uint8_t : the priorty of this service

 Returns
     bool, false if error in initialization, true otherwise

 Description
     Saves away the priority, sets up the initial transition and does any
     other required initialization for this state machine
 Notes

 Author
     J. Edward Carryer, 10/23/11, 18:55
****************************************************************************/
bool InitButtonDBSM(uint8_t Priority)
{
  ES_Event_t ThisEvent;

  MyPriority = Priority;
  //SR Init function initializing Port B , Line 7 (BIT7) to monitor button state
  //Sample button port pin and use it to init last button state
  LastButtonState = Button_Read();
  CurrentState = Debouncing;
  ES_Timer_InitTimer(DB_TIMER, DEBOUNCE_TIME); //Initialize Timer with Debounce Time
  // post the initial transition event
  ThisEvent.EventType = ES_INIT;
  if (ES_PostToService(MyPriority, ThisEvent) == true)
  {
    return true;
  }
  else
  {
    return false;
  }
}

//Public Function
bool CheckButtonEvents(void)
{
  bool    ReturnVal = false;                  //Set initial return value to false
  uint8_t CurrentButtonState = Button_Read(); //reads button state on port b bit 7
  if (CurrentButtonState != LastButtonState)  //if button state changed, Button event occured
  {
    ReturnVal = true;             //Return true to show event occured
    if (CurrentButtonState != 0)  //Button Down event
    {
      ES_Event_t ThisEvent;
      ThisEvent.EventType = ES_BUTTON_DOWN; //post button down event
      ThisEvent.EventParam = 0;             //Dummy parameter
      PostButtonDBSM(ThisEvent);
    }
    else // Button Up event
    {
      ES_Event_t ThisEvent;
      ThisEvent.EventType = ES_BUTTON_UP; //post button up event
      ThisEvent.EventParam = 0;           //Dummy parameter
      PostButtonDBSM(ThisEvent);
    }
  }
  LastButtonState = CurrentButtonState;  //Save Current Button State as Last Button State
  return ReturnVal;
}

/****************************************************************************
 Function
     PostButtonDBSM

 Parameters
     EF_Event ThisEvent , the event to post to the queue

 Returns
     boolean False if the Enqueue operation failed, True otherwise

 Description
     Posts an event to this state machine's queue
 Notes

 Author
     J. Edward Carryer, 10/23/11, 19:25
****************************************************************************/
bool PostButtonDBSM(ES_Event_t ThisEvent)
{
  return ES_PostToService(MyPriority, ThisEvent);
}

/****************************************************************************
 Function
    RunButtonDBSM

 Parameters
   ES_Event : the event to process

 Returns
   ES_Event, ES_NO_EVENT if no error ES_ERROR otherwise

 Description
   add your description here
 Notes
   uses nested switch/case to implement the machine.
 Author
   J. Edward Carryer, 01/15/12, 15:23
****************************************************************************/
ES_Event_t RunButtonDBSM(ES_Event_t ThisEvent)
{
  ES_Event_t ReturnEvent;
  ReturnEvent.EventType = ES_NO_EVENT; // assume no errors

  switch (CurrentState)
  {
    case Debouncing:        // If current state is debouncing
    {
      switch (ThisEvent.EventType)
      {
        case ES_TIMEOUT:  //If event is timeout
        {
          if (ThisEvent.EventParam == DB_TIMER)  //If correct timer timed out
          {
            CurrentState = Ready2Sample;   //Next state Ready2Sample
          }
        }
        break;
      }  // end switch on Debouncing
    }
    case Ready2Sample:     //If current state is Ready2Sample
    {
      switch (ThisEvent.EventType)
      {
        case ES_BUTTON_UP:
        {
          ES_Timer_StartTimer(DB_TIMER);  //Start debounce Timer
          CurrentState = Debouncing;      //next state is Debouncing
          PostMorseElementsSM(ThisEvent); //Post event to MorseElements
          PostDecodeMorse(ThisEvent);     //Post to DecodeMorse
        }
        break;
        case ES_BUTTON_DOWN:
        {
          ES_Timer_StartTimer(DB_TIMER);  //Start debounce Timer
          CurrentState = Debouncing;      //Next state is debouncing
          PostMorseElementsSM(ThisEvent); //Post event to MorseElements
          PostDecodeMorse(ThisEvent);     //Post to DecodeMorse
        }
        break;
      }
    }

    break;
  }                                   // end switch on Current State
  return ReturnEvent;
}

/****************************************************************************
 Function
     QueryButtonDBSM

 Parameters
     None

 Returns
     TemplateState_t The current state of the Template state machine

 Description
     returns the current state of the Template state machine
 Notes

 Author
     J. Edward Carryer, 10/23/11, 19:21
****************************************************************************/
ButtonDBState_t QueryButtonDBSM(void)
{
  return CurrentState;
}

/***************************************************************************
 private functions
 ***************************************************************************/
