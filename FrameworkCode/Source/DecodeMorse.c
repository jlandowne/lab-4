/****************************************************************************
 Module
   DecodeMorse.c

 Revision
   1.0.1

 Description
   This is a template file for implementing a simple service under the
   Gen2 Events and Services Framework.

 Notes

 History
 When           Who     What/Why
 -------------- ---     --------
 01/16/12 09:58 jec      began conversion from TemplateFSM.c
****************************************************************************/
/*----------------------------- Include Files -----------------------------*/
/* include header files for this state machine as well as any machines at the
   next lower level in the hierarchy that are sub-machines to this machine
*/
#include "ES_Configure.h"
#include "ES_Framework.h"
#include "DecodeMorse.h"
#include "LCDService.h"
#include <string.h>

/*----------------------------- Module Defines ----------------------------*/
#define LENGTH 8

/*---------------------------- Module Functions ---------------------------*/
/* prototypes for private functions for this service.They should be functions
   relevant to the behavior of this service
*/
static char MorseString[LENGTH];
static char LegalChars[] = "ABCDEFGHIJKLMNOPQRSTUVWXYZ1234567890?.,:'-/()\"= !$&+;@_";
static char MorseCode[][8] = { ".-", "-...", "-.-.", "-..", ".", "..-.", "--.", "....", "..", ".---", "-.-", ".-..",
                               "--", "-.", "---", ".--.", "--.-", ".-.", "...", "-", "..-", "...-", ".--", "-..-", "-.--", "--..", ".----", "..---",
                               "...--", "....-", ".....", "-....", "--...", "---..", "----.", "-----", "..--..", ".-.-.-", "--..--", "---...",
                               ".----.", "-....-", "-..-.", "-.--.-", "-.--.-", ".-..-.", "-...-", "-.-.--", "...-..-", ".-...", ".-.-.", "-.-.-.",
                               ".--.-.", "..--.-" };

/*---------------------------- Module Variables ---------------------------*/
// with the introduction of Gen2, we need a module level Priority variable
static uint8_t MyPriority;

/*------------------------------ Module Code ------------------------------*/
/****************************************************************************
 Function
     InitDecodeMorse

 Parameters
     uint8_t : the priorty of this service

 Returns
     bool, false if error in initialization, true otherwise

 Description
     Saves away the priority, and does any
     other required initialization for this service
 Notes

 Author
     J. Edward Carryer, 01/16/12, 10:00
****************************************************************************/
bool InitDecodeMorse(uint8_t Priority)
{
  ES_Event_t ThisEvent;

  MyPriority = Priority;
  /********************************************
   in here you write your initialization code
   *******************************************/
  //Clear MorseString var
  for (int i = 0; i < LENGTH; i++)
  {
    MorseString[i] = '\0';
  }
  // post the initial transition event
  ThisEvent.EventType = ES_INIT;
  if (ES_PostToService(MyPriority, ThisEvent) == true)
  {
    return true;
  }
  else
  {
    return false;
  }
}

//Private Function Decode Morse String
static char DecodeMorseString(void)
{
  char ReturnVar;
  for (int i = 0; i < strlen(LegalChars); i++) //For every entry in the array Legal Char array
  {
    if (strcmp(MorseString, MorseCode[i]) == 0)  //If MorseString is the same as current position in MorseCode
    {
      printf( "MorseString = %s\r\n", MorseString);
      printf( "Code = %s\r\n\n\n",    MorseCode[i]);
      ReturnVar = LegalChars[i];  //Get character corresponding to Morse Code
      return ReturnVar;           //Return variable
    }
  }
  return '~'; //return '~', since we didn't find a matching string in MorseCode
}

/****************************************************************************
 Function
     PostDecodeMorse

 Parameters
     EF_Event ThisEvent ,the event to post to the queue

 Returns
     bool false if the Enqueue operation failed, true otherwise

 Description
     Posts an event to this state machine's queue
 Notes

 Author
     J. Edward Carryer, 10/23/11, 19:25
****************************************************************************/
bool PostDecodeMorse(ES_Event_t ThisEvent)
{
  return ES_PostToService(MyPriority, ThisEvent);
}

/****************************************************************************
 Function
    RunDecodeMorse

 Parameters
   ES_Event : the event to process

 Returns
   ES_Event, ES_NO_EVENT if no error ES_ERROR otherwise

 Description
   add your description here
 Notes

 Author
   J. Edward Carryer, 01/15/12, 15:23
****************************************************************************/
ES_Event_t RunDecodeMorse(ES_Event_t ThisEvent)
{
  ES_Event_t ReturnEvent;
  ReturnEvent.EventType = ES_NO_EVENT; // assume no errors
  /********************************************
   in here you write your service code
   *******************************************/

  switch (ThisEvent.EventType)
  {
    case ES_DOT_DETECTED: //If ThisEvent is DotDetected Event
    {
      //If there is room for another Morse element in the internal representation
      if (strlen(MorseString) < LENGTH)
      {
        //Add a Dot to the internal representation
        strcat(MorseString, ".");
      }
      else
      {
        //Set ReturnValue to ES_ERROR with param set to indicate this location
        ReturnEvent.EventType = ES_ERROR;
        ReturnEvent.EventParam = strlen(MorseString);
      }
    }
    break;
    case ES_DASH_DETECTED: //If ThisEvent is DotDetected Event
    {
      //If there is room for another Morse element in the internal representation
      if (strlen(MorseString) < LENGTH)
      {
        //Add a Dash to the internal representation
        strcat(MorseString, "-");
      }
      else
      {
        //Set ReturnValue to ES_ERROR with param set to indicate this location
        ReturnEvent.EventType = ES_ERROR;
        ReturnEvent.EventParam = strlen(MorseString);
      }
      break;
      case ES_EOC_DETECTED://If ThisEvent is EOC Detected Event
      {
        //call DecodeMorse to try and match current MorseString
        char c = DecodeMorseString();

        //Print to LCD the decoded character
        ES_Event_t CharEvent;
        CharEvent.EventType = ES_LCD_PUTCHAR; //PutChar event to write to LCD screen
        CharEvent.EventParam = c;             //Parameter is the char to write
        PostLCDService(CharEvent);
        //Clear MorseString var
        for (int i = 0; i < LENGTH; i++)
        {
          MorseString[i] = '\0';
        }
      }
      break;
      case ES_EOW_DETECTED://If ThisEvent is EOW Detected Event
      {
        //call DecodeMorse to try and match current MorseString
        char c = DecodeMorseString();

        //If no error
        //Print to LCD the decoded character
        ES_Event_t CharEvent;
        CharEvent.EventType = ES_LCD_PUTCHAR;
        CharEvent.EventParam = c;
        PostLCDService(CharEvent);

        //Print a SPACE to LCD between words
        ES_Event_t SpaceEvent;
        SpaceEvent.EventType = ES_LCD_PUTCHAR;
        SpaceEvent.EventParam = ' ';
        PostLCDService(SpaceEvent);

        //Clear MorseString var
        for (int i = 0; i < LENGTH; i++)
        {
          MorseString[i] = '\0';
        }
      }
      break;
      case ES_BAD_SPACE://If ThisEvent is BadSpace Event
      {
        //Clear MorseString var
        for (int i = 0; i < LENGTH; i++)
        {
          MorseString[i] = '\0';
        }
      }
      break;
      case ES_BAD_PULSE://If ThisEvent is BadPulse Event
      {
        //Clear MorseString var
        for (int i = 0; i < LENGTH; i++)
        {
          MorseString[i] = '\0';
        }
      }
      break;
      case ES_BUTTON_DOWN: //If ThisEvent is Button Down Event
      {
        //Clear MorseString var
        for (int i = 0; i < LENGTH; i++)
        {
          MorseString[i] = '\0';
        }
      }
      break;
    }
  }
  return ReturnEvent;
}

/***************************************************************************
 private functions
 ***************************************************************************/

/*------------------------------- Footnotes -------------------------------*/
/*------------------------------ End of file ------------------------------*/
