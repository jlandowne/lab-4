/****************************************************************************
 Module
   TemplateFSM.c

 Revision
   1.0.1

 Description
   This is a template file for implementing flat state machines under the
   Gen2 Events and Services Framework.

 Notes

 History
 When           Who     What/Why
 -------------- ---     --------
 01/15/12 11:12 jec      revisions for Gen2 framework
 11/07/11 11:26 jec      made the queue static
 10/30/11 17:59 jec      fixed references to CurrentEvent in RunTemplateSM()
 10/23/11 18:20 jec      began conversion from SMTemplate.c (02/20/07 rev)
****************************************************************************/
/*----------------------------- Include Files -----------------------------*/
/* include header files for this state machine as well as any machines at the
   next lower level in the hierarchy that are sub-machines to this machine
*/
#include "ES_Configure.h"
#include "ES_Framework.h"
#include "MorseElements.h"
#include "ShiftRegisterWrite.h"
#include "DecodeMorse.h"

/*----------------------------- Module Defines ----------------------------*/

/*---------------------------- Module Functions ---------------------------*/
/* prototypes for private functions for this machine.They should be functions
   relevant to the behavior of this state machine
*/

/*---------------------------- Module Variables ---------------------------*/
// everybody needs a state variable, you may need others as well.
// type of state variable should match htat of enum in header file
static MorseElementsState_t CurrentState;
static uint8_t              LastInputState = 0;
static uint16_t             TimeOfLastRise = 0;
static uint16_t             TimeOfLastFall = 0;
static uint16_t             LengthOfDot = 0;
static uint16_t             FirstDelta = 0;

// with the introduction of Gen2, we need a module level Priority var as well
static uint8_t MyPriority;

/*------------------------------ Module Code ------------------------------*/
/****************************************************************************
 Function
     InitMorseElementsFSM

 Parameters
     uint8_t : the priorty of this service

 Returns
     bool, false if error in initialization, true otherwise

 Description
     Saves away the priority, sets up the initial transition and does any
     other required initialization for this state machine
 Notes

 Author
     J. Edward Carryer, 10/23/11, 18:55
****************************************************************************/
bool InitializeMorseElementsSM(uint8_t Priority)
{
  ES_Event_t ThisEvent;

  MyPriority = Priority;
  // put us into the Initial PseudoState
  CurrentState = InitMorseElements;
  // post the initial transition event
  ThisEvent.EventType = ES_INIT;
  if (ES_PostToService(MyPriority, ThisEvent) == true)
  {
    return true;
  }
  else
  {
    return false;
  }
}

bool CheckMorseElements(void)
{
  bool    ReturnVal = false;
  uint8_t CurrentInputState = SR_Read();    //reads Input Pin (Port B Bit 3)
  if (LastInputState != CurrentInputState)  //if input changed, event occured
  {
    if (CurrentInputState != 0)  //If input high, rising edge
    {
      ES_Event_t  ThisEvent;
      ThisEvent.EventType = ES_RISING_EDGE;         //post rising edge event
      uint16_t    CurrentTime = ES_Timer_GetTime(); //gets time of rise as event parameter
      ThisEvent.EventParam = CurrentTime;
      PostMorseElementsSM(ThisEvent); //Post to MorseElements
    }
    else   //f input low, falling edge
    {
      ES_Event_t  ThisEvent;
      ThisEvent.EventType = ES_FALLING_EDGE;        //post falling edge event
      uint16_t    CurrentTime = ES_Timer_GetTime(); //gets time of rise as event parameter
      ThisEvent.EventParam = CurrentTime;
      PostMorseElementsSM(ThisEvent); //Post to MorseElements
    }
    ReturnVal = true; //Return true that event occured
  }
  LastInputState = CurrentInputState;
  return ReturnVal;
}

//Private Function
static void CharacterizeSpace(void)
{
//Local Variables
  uint16_t    LastInterval;
  ES_Event_t  Event2Post;

//Calculate LastInterval as TimeOfLastRise – TimeOfLastFall
  LastInterval = TimeOfLastRise - TimeOfLastFall;

//If LastInterval not OK for a Dot Space
  if (LastInterval > 2 * LengthOfDot)
  {
    //If LastInterval OK for a Character Space
    if ((LastInterval >= (3 * (LengthOfDot - 10))) && (LastInterval <= (3 * (LengthOfDot + 10))))
    {
      //PostEvent EOCDetected Event to Decode Morse Service & Morse Elements Service
      Event2Post.EventType = ES_EOC_DETECTED;
      Event2Post.EventParam = 0;        //Dummy parameter
      PostMorseElementsSM(Event2Post);  //Post event to MorseElements
      PostDecodeMorse(Event2Post);      //Post to DecodeMorse
    }
    else
    {
      //If LastInterval OK for word space
      if ((LastInterval >= (7 * (LengthOfDot - 10))) && (LastInterval <= (7 * (LengthOfDot + 10))))
      {
        //PostEvent EOWDetected Event to Decode Morse Service
        Event2Post.EventType = ES_EOW_DETECTED;
        Event2Post.EventParam = 0;    //Dummy parameter
        PostDecodeMorse(Event2Post);  //Post to DecodeMorse
      }
      else
      {
        //PostEvent BadSpace Event to Decode Morse Service
        Event2Post.EventType = ES_BAD_SPACE;
        Event2Post.EventParam = 0;    //Dummy parameter
        PostDecodeMorse(Event2Post);  //Post to DecodeMorse
      }
    }
  }
}

static void CharacterizePulse(void)
{
//Local Variables
  uint16_t    LastPulseWidth;
  ES_Event_t  Event2Post;

  //Calculate LastPulseWidth as TimeOfLastFall - TimeOfLastRise
  LastPulseWidth = TimeOfLastFall - TimeOfLastRise;
  //If LastPulseWidth OK for a dot (range of values)
  if ((LastPulseWidth >= (LengthOfDot - 2)) && (LastPulseWidth <= (LengthOfDot + 2)))
  {
    //PostEvent DotDetected Event to Decode Morse Service
    Event2Post.EventType = ES_DOT_DETECTED;
    Event2Post.EventParam = 0;    //Dummy parameter
    PostDecodeMorse(Event2Post);  //Post to DecodeMorse
  }
  else
  {
    //If LastPulseWidth OK for dash (Range of values)
    if ((LastPulseWidth >= (3 * (LengthOfDot - 2))) && (LastPulseWidth <= (3 * (LengthOfDot + 2))))
    {
      //PostEvent DashDetected Event to Decode Morse Service
      Event2Post.EventType = ES_DASH_DETECTED;
      Event2Post.EventParam = 0;    //Dummy parameter
      PostDecodeMorse(Event2Post);  //Post to DecodeMorse
    }
    else
    {
      //PostEvent BadPulse Event to Decode Morse Service
      Event2Post.EventType = ES_BAD_PULSE;
      Event2Post.EventParam = 0;    //Dummy parameter
      PostDecodeMorse(Event2Post);  //Post to DecodeMorse
    }
  }
}

static void TestCalibration(void)
{
  uint16_t SecondDelta;
  if (FirstDelta == 0)   //If calibration is just starting (FirstDelta is 0)
  {
    //Set FirstDelta to most recent pulse width
    FirstDelta = TimeOfLastFall - TimeOfLastRise;
  }
  else
  {
    // Set SecondDelta to most recent pulse width
    SecondDelta = TimeOfLastFall - TimeOfLastRise;
    // If cvlid ratios for dot:dash
    if ((100.0 * FirstDelta / SecondDelta) <= 33.33)
    {
      // Save FirstDelta as LengthOfDot
      LengthOfDot = FirstDelta;
      ES_Event_t ThisEvent;
      ThisEvent.EventType = ES_CALIBRATION_COMPLETE;  //post Cal complete to MorseElements
      ThisEvent.EventParam = 0;                       //dummy parameter
      PostMorseElementsSM(ThisEvent);
    }
    else if ((100.0 * FirstDelta / SecondDelta) > 300.0)
    {
      // Save SecondDelta as LengthOfDot
      LengthOfDot = SecondDelta;
      ES_Event_t ThisEvent;
      ThisEvent.EventType = ES_CALIBRATION_COMPLETE;  //post Cal complete to MorseElements
      ThisEvent.EventParam = 0;                       //dummy parameter
      PostMorseElementsSM(ThisEvent);
    }
    else   //prepare for next pulse
    {
      FirstDelta = SecondDelta;
    }
  }
}

/****************************************************************************
 Function
     PostMorseElementsSM

 Parameters
     EF_Event ThisEvent , the event to post to the queue

 Returns
     boolean False if the Enqueue operation failed, True otherwise

 Description
     Posts an event to this state machine's queue
 Notes

 Author
     J. Edward Carryer, 10/23/11, 19:25
****************************************************************************/
bool PostMorseElementsSM(ES_Event_t ThisEvent)
{
  return ES_PostToService(MyPriority, ThisEvent);
}

/****************************************************************************
 Function
    RunTemplateFSM

 Parameters
   ES_Event : the event to process

 Returns
   ES_Event, ES_NO_EVENT if no error ES_ERROR otherwise

 Description
   add your description here
 Notes
   uses nested switch/case to implement the machine.
 Author
   J. Edward Carryer, 01/15/12, 15:23
****************************************************************************/
ES_Event_t RunMorseElementsSM(ES_Event_t ThisEvent)
{
  ES_Event_t ReturnEvent;
  ReturnEvent.EventType = ES_NO_EVENT; // assume no errors

  switch (CurrentState)
  {
    case InitMorseElements:        // If current state is initial Psedudo State
    {
      if (ThisEvent.EventType == ES_INIT)    // only respond to ES_Init
      {
        CurrentState = CalWaitForRise;  //Next state is CalWaitForRise
      }
    }
    break;

    case CalWaitForRise:        // If current state is CalWaitForRise
    {
      switch (ThisEvent.EventType)
      {
        case ES_RISING_EDGE:  //If event Rising Edge

        {
          //Set time of last rise to time from event param
          TimeOfLastRise = ThisEvent.EventParam;
          CurrentState = CalWaitForFall;  //set next state to CalWaitForFall
        }
        break;
        case ES_CALIBRATION_COMPLETE: //If Calibration complete event
        {
          puts("Cal Complete!\n\r");
          printf("LoD = %d\n\r", LengthOfDot);
          CurrentState = EOC_WaitRise; //next state is EOC_WaitRise
        }
        break;
      }  // end switch on CalWaitForRise
    }
    break;
    case CalWaitForFall:       // If current state is CalWaitforFall
    {
      switch (ThisEvent.EventType)
      {
        case ES_FALLING_EDGE:  //If event is Rising Edge
        {
          //Set TimeOfLastFall to Time from event parameter
          TimeOfLastFall = ThisEvent.EventParam;
          CurrentState = CalWaitForRise;  //Next state CalWaitForRise
          TestCalibration();              //Call Test Calibration
        }
        break;
      }
    }
    break;
    case EOC_WaitRise:  // If current state is EOC_WaitRise
    {
      switch (ThisEvent.EventType)
      {
        case ES_RISING_EDGE: //If Rising Edge event
        {
          //Set TimeOfLastRise to Time from event parameter
          TimeOfLastRise = ThisEvent.EventParam;
          CurrentState = EOC_WaitFall;  //Next state EOC_WaitForFall
          CharacterizeSpace();          //Call Characterize Space Function
        }
        break;
        case ES_BUTTON_DOWN: //if Button Down event
        {
          CurrentState = CalWaitForRise;  //next state CalWaitForRise
          FirstDelta = 0;                 //New Calibration params
        }
        break;
      }
    }
    break;
    case EOC_WaitFall: // If current state is  EOC_WaitFall
    {
      switch (ThisEvent.EventType)
      {
        case ES_FALLING_EDGE: //If Falling Edge event
        {
          //Set TimeOfLastFall to Time from event parameter
          TimeOfLastFall = ThisEvent.EventParam;
          CurrentState = EOC_WaitRise;  //Next state CalWaitForRise
        }
        break;
        case ES_BUTTON_DOWN: //If Button Down event
        {
          CurrentState = CalWaitForRise;  //next state CalWaitForRise
          FirstDelta = 0;                 //New Calibration params
        }
        break;
        case ES_EOC_DETECTED: //if EOC event
        {
          CurrentState = DecodeWaitFall; //next state DecodeWaitFall
        }
        break;
      }
    }
    break;
    case DecodeWaitRise:  // If current state is  DecodeWaitRise
    {
      switch (ThisEvent.EventType)
      {
        case ES_RISING_EDGE: //If Rising Edge event
        {
          //Set TimeOfLastRise to Time from event parameter
          TimeOfLastRise = ThisEvent.EventParam;
          CurrentState = DecodeWaitFall;  //Next state DecideWautFall
          CharacterizeSpace();            //Call CharacterizeSpace function
        }
        break;
        case ES_BUTTON_DOWN:  //If ButtonDown Event
        {
          CurrentState = CalWaitForRise;  //next state CalWaitForRise
          FirstDelta = 0;                 //New Calibration params
        }
        break;
      }
    }
    break;
    case DecodeWaitFall:  // If current state is  DecodeWaitFall
    {
      switch (ThisEvent.EventType)
      {
        case ES_FALLING_EDGE: //If falling Edge event
        {
          //Set TimeOfLastFall to Time from event parameter
          TimeOfLastFall = ThisEvent.EventParam;
          CurrentState = DecodeWaitRise;  //Next state Decode
          CharacterizePulse();            //Call CharacterizePulse function
        }
        break;
        case ES_BUTTON_DOWN: //If Button Down event
        {
          CurrentState = CalWaitForRise;  //next state CalWaitForRise
          FirstDelta = 0;                 //New Calibration params
        }
        break;
      }
    }
    break;
  }

  return ReturnEvent;
}

/***************************************************************************
 private functions
 ***************************************************************************/
