/****************************************************************************
 Module
   ShiftRegisterWrite.c

 Revision
   1.0.1

 Description
   This module acts as the low level interface to a write only shift register.

 Notes

 History
 When           Who     What/Why
 -------------- ---     --------
 10/11/15 19:55 jec     first pass

****************************************************************************/
// the common headers for C99 types
#include <stdint.h>
#include <stdbool.h>

// the headers to access the GPIO subsystem
#include "inc/hw_memmap.h"
#include "inc/hw_types.h"
#include "inc/hw_gpio.h"
#include "inc/hw_sysctl.h"

// the headers to access the TivaWare Library
#include "driverlib/sysctl.h"
#include "driverlib/pin_map.h"
#include "driverlib/gpio.h"
#include "driverlib/timer.h"
#include "driverlib/interrupt.h"

#include "BITDEFS.H"

// readability defines
#define DATA GPIO_PIN_0

#define SCLK GPIO_PIN_1
#define SCLK_HI BIT1HI
#define SCLK_LO BIT1LO

#define RCLK GPIO_PIN_2
#define RCLK_LO BIT2LO
#define RCLK_HI BIT2HI
#define ALL_BITS (0xff << 2)

#define GET_MSB_IN_LSB(x) ((x & 0x80) >> 7)

// an image of the last 8 bits written to the shift register
static uint8_t LocalRegisterImage = 0;

// Set up Bits 0-2 as output and set RCLK as Hi and SLK and Data as Lo
void SR_Init(void)
{
  // set up port B by enabling the peripheral clock, waiting for the
  // peripheral to be ready and setting the direction
  // of PB0, PB1 & PB2 to output

  // start with the data & sclk lines low and the RCLK line high

  HWREG(SYSCTL_RCGCGPIO) |= BIT1HI; //enable Port B

  while ((HWREG(SYSCTL_PRGPIO) & SYSCTL_PRGPIO_R1) != SYSCTL_PRGPIO_R1) //wait for Port B to initialize
  {}

  HWREG(GPIO_PORTB_BASE + GPIO_O_DEN) |= BIT0HI;  //assign bit 0 as digital I/O
  HWREG(GPIO_PORTB_BASE + GPIO_O_DIR) |= BIT0HI;  //set bit 0 as output
  HWREG(GPIO_PORTB_BASE + GPIO_O_DEN) |= BIT1HI;  //assign bit 1 as digital I/O
  HWREG(GPIO_PORTB_BASE + GPIO_O_DIR) |= BIT1HI;  //set bit 1 as output
  HWREG(GPIO_PORTB_BASE + GPIO_O_DEN) |= BIT2HI;  //assign bit 2 as digital I/O
  HWREG(GPIO_PORTB_BASE + GPIO_O_DIR) |= BIT2HI;  //set bit 2 as output
  HWREG(GPIO_PORTB_BASE + GPIO_O_DEN) |= BIT3HI;  //assign bit 3 as digital I/O (Signal Read)
  HWREG(GPIO_PORTB_BASE + GPIO_O_DIR) &= BIT3LO;  //set bit 3 as input
  HWREG(GPIO_PORTB_BASE + GPIO_O_DEN) |= BIT7HI;  //assign bit 7 as digital I/O (Button Read)
  HWREG(GPIO_PORTB_BASE + GPIO_O_DIR) &= BIT7LO;  //set bit  as input

  HWREG(GPIO_PORTB_BASE + (GPIO_O_DATA + ALL_BITS)) &= (BIT0LO & SCLK_LO);  //SetData and CLK Lo
  HWREG(GPIO_PORTB_BASE + (GPIO_O_DATA + ALL_BITS)) |= RCLK_HI;             //Set Register Hi
}

// Returns value of current shift register
uint8_t SR_GetCurrentRegister(void)
{
  return LocalRegisterImage;
}

// Write input value to shift register
void SR_Write(uint8_t NewValue)
{
  uint8_t BitCounter;
  LocalRegisterImage = NewValue; // save a local copy

// lower the register clock
  HWREG(GPIO_PORTB_BASE + (GPIO_O_DATA + ALL_BITS)) &= RCLK_LO;
// shift out the data while pulsing the serial clock
// Isolate the MSB of NewValue, put it into the LSB position and output to port
  uint8_t bt; //current LSB
  for (BitCounter = 0; BitCounter < 8; BitCounter++)
  {
    bt = GET_MSB_IN_LSB(NewValue);
    /*if (bt == 0) {
      HWREG(GPIO_PORTB_BASE+(GPIO_O_DATA + ALL_BITS)) &= BIT0LO; //sets Data line to current Lo if bt is Lo
    }
    else {
      HWREG(GPIO_PORTB_BASE+(GPIO_O_DATA + ALL_BITS)) |= BIT0HI; //sets Data line to current HI if bt is HI
    } */
    HWREG(GPIO_PORTB_BASE + (GPIO_O_DATA + ALL_BITS)) = (HWREG(GPIO_PORTB_BASE + (GPIO_O_DATA + ALL_BITS)) & BIT0LO) | bt; //sets Data line to current Lo if bt is Lo, hi if bt is hi

    // raise SCLK
    HWREG(GPIO_PORTB_BASE + (GPIO_O_DATA + ALL_BITS)) |= SCLK_HI;
// lower SCLK
    HWREG(GPIO_PORTB_BASE + (GPIO_O_DATA + ALL_BITS)) &= SCLK_LO;
// finish looping through bits in NewValue
    NewValue = NewValue << 1; //Shift left so next bit is MSB
  }

// raise the register clock to latch the new data
  HWREG(GPIO_PORTB_BASE + (GPIO_O_DATA + ALL_BITS)) |= RCLK_HI;
}

//Public Function that reads the state of Port B bit 3 (Input Signal) and returns that value
uint8_t SR_Read()
{
  uint8_t ReturnValue;                                                      //Init local variable
  ReturnValue = HWREG(GPIO_PORTB_BASE + (GPIO_O_DATA + ALL_BITS)) & BIT3HI; //reads value on input signal (Pin 3)
  return ReturnValue;                                                       //Returns value
}

uint8_t Button_Read() //Public Function that reads the state of Port B bit 7 (Button) and returns that value
{
  uint8_t ReturnValue;                                                      //Init local variable
  ReturnValue = HWREG(GPIO_PORTB_BASE + (GPIO_O_DATA + ALL_BITS)) & BIT7HI; //reads value on Button (Pin 7)
  return ReturnValue;                                                       //Returns value
}
