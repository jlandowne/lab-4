#define TEST
/* test function 1 for CodeWarrior Tutorial */
#include <stdio.h>
#include <stdint.h>
#include <stdbool.h>
#include "LCD_Write.h"

#include "inc/hw_types.h"
#include "inc/hw_memmap.h"
#include "driverlib/sysctl.h"

#include "ES_Configure.h"
#include "ES_Framework.h"
#include "ES_Port.h"
#include "termio.h"
#include "EnablePA25_PB23_PD7_PF0.h"

#define clrScrn() printf("\x1b[2J")
#define goHome() printf("\x1b[1,1H")
#define clrLine() printf("\x1b[K")
#include "ShiftRegisterWrite.h"
#include "MorseElements.h"

// a prototype is not really necessary here,
// but I want to set a good example

#ifdef TEST
/* test Harness for testing this module */
#include "termio.h"
#endif
int main(void)
{
  SysCtlClockSet(SYSCTL_SYSDIV_5 | SYSCTL_USE_PLL | SYSCTL_OSC_MAIN | SYSCTL_XTAL_16MHZ);
  TERMIO_Init();
  clrScrn();
  clrLine();

  puts("Start");
  SR_Init();
  while (!kbhit())
  {
    CheckMorseElements();
  }
  TestCalibration();
  puts("End");

  return 0;
}